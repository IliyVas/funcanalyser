#pragma once

#include <clang-c/Index.h>
#include <vector>
#include "Analyser.h"
#include "Graph.h"

class FileAnalyser : public Analyser
{
public:
	FileAnalyser(const char *filepath);
	~FileAnalyser();

	// Check if translation unit has been successfully created.
	bool IsTUCreated() const;

	// Check if translation unit has any syntax errors.
	bool IsTUHasErrors() const;

	// Find potentially recursive functions in the file.
	// If translation unit has any syntax errors this 
	// method sends a warning.
	std::vector<std::string> FindPotentiallyRecFunc() const;
	
	// Perform full file analysis if translation unit has been 
	// successfully created and it hasn't any syntax errors.
	void Analyze();

private:
	CXIndex fa_index;
	CXTranslationUnit fa_unit;
	bool enableWarnings;
};

struct VisitorClientData
{
	std::shared_ptr<DirectedGraph<std::string>> FuncCallGraph;
	node_ptr_t<std::string> CurrentDeclFuncNode;

	VisitorClientData(
		std::shared_ptr<DirectedGraph<std::string>>& graph, 
		node_ptr_t<std::string>& funcNode
	);
};

CXChildVisitResult CreateFuncCallGraphVisitor(CXCursor cursor, CXCursor parent,
	CXClientData clientData);

CXChildVisitResult FindFuncDeclVisitor(CXCursor cursor, CXCursor parent,
	CXClientData clientData);

