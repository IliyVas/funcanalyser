#pragma once

class Analyser
{
public:
	virtual ~Analyser()
	{}
	virtual void Analyze() = 0;
};