#include "FileAnalyser.h"
#include "Graph.h"
#include <string>
#include <iostream>


FileAnalyser::FileAnalyser(const char* filepath) : enableWarnings(true)
{
	fa_index = clang_createIndex(0, 0);

	const char* args[] = { "-c" };
	int numArgs = sizeof(args) / sizeof(*args);

	fa_unit = clang_parseTranslationUnit(
		fa_index, 
		filepath, 
		args, 
		numArgs, 
		nullptr, 
		0, 
		CXTranslationUnit_None
	);
}

FileAnalyser::~FileAnalyser()
{
	clang_disposeTranslationUnit(fa_unit);
	clang_disposeIndex(fa_index);
}

bool FileAnalyser::IsTUCreated() const
{
	return fa_unit != nullptr;
}

bool FileAnalyser::IsTUHasErrors() const
{
	unsigned diagnosticCount = clang_getNumDiagnostics(fa_unit);

	for (unsigned i = 0; i < diagnosticCount; i++) {
		auto diagnostic = clang_getDiagnostic(fa_unit, i);
		auto severity = clang_getDiagnosticSeverity(diagnostic);

		if (severity == CXDiagnostic_Error || severity == CXDiagnostic_Fatal)
			return true;
	}

	return false;
}

std::vector<std::string> FileAnalyser::FindPotentiallyRecFunc() const
{
	auto result = std::vector<std::string>();

	if(!IsTUCreated())
	{
		std::cerr 
			<< "Translation unit hasn't been created. Can't perform search."
			<< std::endl;
		return result;
	}

	if (enableWarnings && IsTUHasErrors())
	{
		std::cerr
			<< "Translation unit has some syntax errors."
			<< std::endl
			<< "The result may be different from analysis result of file with no errors."
			<< std::endl;
	}

	auto cursor = clang_getTranslationUnitCursor(fa_unit);
	auto g = new DirectedGraph<std::string>();

	clang_visitChildren(cursor, FindFuncDeclVisitor, g);
	auto components = g->FindStronglyConnectedComp();

	for (auto comp_it = components.begin(); comp_it != components.end(); ++comp_it)
	{
		auto nodes = (*comp_it).GetNodes();

		for (auto node_it = nodes.begin(); node_it != nodes.end(); ++node_it)
			result.push_back((*node_it)->GetData());
	}

	return result;
}

void FileAnalyser::Analyze()
{
	if (!IsTUCreated())
	{
		std::cerr 
			<< "Translation unit hasn't been created. Can't perform analysis."
			<< std::endl;
		return;
	}

	if (IsTUHasErrors())
	{
		std::cerr
			<< "Translation unit has some syntax errors."
			<< std::endl
			<< "Analysis hasn't been performed."
			<< std::endl;
		return;
	}

	enableWarnings = false;

	auto recFunc = FindPotentiallyRecFunc();
	if (recFunc.size() == 0)
		std::cout
			<< "There are no potentially recursive functions in file."
			<< std::endl;

	else
	{
		std::cout << "Potentially recursive functions names:"	<< std::endl;

		for (unsigned int i = 0; i < recFunc.size(); ++i)
			std::cout << (i + 1) << ") " << recFunc[i] << std::endl;
	}

	enableWarnings = true;
}


VisitorClientData::VisitorClientData(
	std::shared_ptr<DirectedGraph<std::string>>& graph,
	node_ptr_t<std::string>& funcNode
) : FuncCallGraph(graph), CurrentDeclFuncNode(funcNode)
{}

CXChildVisitResult FindFuncDeclVisitor(CXCursor cursor, CXCursor parent,
	CXClientData clientData)
{
	auto curType = clang_getCursorKind(cursor);
	auto parType = clang_getCursorKind(parent);

	if (curType == CXCursor_CompoundStmt && parType == CXCursor_FunctionDecl)
	{
		auto funcCName = clang_getCString(clang_getCursorSpelling(parent));
		auto funcName = std::string(funcCName);

		auto graph_ptr = static_cast<DirectedGraph<std::string>*>(clientData);
		auto graph_shr_ptr = std::shared_ptr<DirectedGraph<std::string>>(graph_ptr);

		auto funcNodes = graph_ptr->FindNodes(funcName);
		node_ptr_t<std::string> funcNode_ptr;

		if (funcNodes.size() == 0)
			funcNode_ptr = graph_ptr->AddNode(funcName);
		else
			funcNode_ptr = funcNodes[0];

		auto data = new VisitorClientData(graph_shr_ptr, funcNode_ptr);

		clang_visitChildren(cursor, CreateFuncCallGraphVisitor, data);

		return CXChildVisit_Continue;
	}
	
	return CXChildVisit_Recurse;
}

CXChildVisitResult CreateFuncCallGraphVisitor(CXCursor cursor, CXCursor parent,
	CXClientData clientData)
{
	if(clang_getCursorKind(cursor) == CXCursor_CallExpr)
	{
		auto callFuncCName = clang_getCString(clang_getCursorSpelling(cursor));
		auto callFuncName = std::string(callFuncCName);

		auto visitorData = static_cast<VisitorClientData*>(clientData);
		auto graph_ptr = visitorData->FuncCallGraph;
		auto funcNodes = graph_ptr->FindNodes(callFuncName);

		node_ptr_t<std::string> callFuncNode_ptr;

		if (funcNodes.size() == 0)
			callFuncNode_ptr = graph_ptr->AddNode(callFuncName);
		else
			callFuncNode_ptr = funcNodes[0];

		graph_ptr->TryAddEdge(visitorData->CurrentDeclFuncNode, callFuncNode_ptr);
	}
	
	return CXChildVisit_Recurse;
}