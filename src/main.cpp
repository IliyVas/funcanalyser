#include "FileAnalyser.h"
#include <iostream>
#include <string>

int main()
{
	std::string path;
	
	std::cout << "Input file: ";
	std::getline(std::cin, path);

	Analyser* fa = new FileAnalyser(path.c_str());
	fa->Analyze();

	system("pause");
	return 0;
}