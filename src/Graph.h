#pragma once

#include <memory>
#include <list>
#include <map>
#include <stack>
#include <algorithm>

template <typename T>
class Node
{
public:
	explicit Node(const T &data);

	T GetData();

private:
	T n_data;
};

template <typename T>
using node_ptr_t = std::shared_ptr<Node<T>>;

template <typename T>
using successors_t = std::list<node_ptr_t<T>>;

template <typename T>
class DirectedGraph
{
public:
	DirectedGraph();

	node_ptr_t<T> AddNode(const T &data);
	bool TryAddEdge(node_ptr_t<T> from, node_ptr_t<T> to);
	std::list<node_ptr_t<T>> GetNodes() const;
	std::vector<node_ptr_t<T>> FindNodes(const T &data) const;

	// Tarjan's algorithm
	std::vector<DirectedGraph<T>> FindStronglyConnectedComp() const;

private:
	std::list<std::shared_ptr<Node<T>>> dg_nodes;
	std::map<node_ptr_t<T>, successors_t<T>> dg_edges;

	bool isNodeInGraph(node_ptr_t<T> node) const;
	void addNode(node_ptr_t<T> node);

	struct nodeMeta
	{
		nodeMeta() {};
		nodeMeta(unsigned int index, unsigned int llink, bool onStack);

		unsigned int Index;
		unsigned int Lowlink;
		bool OnStack;
	};

	DirectedGraph<T> stronglyConnected(
		node_ptr_t<T> node, 
		std::map<node_ptr_t<T>, std::shared_ptr<nodeMeta>>& nodesMeta,
		unsigned int& index, 
		std::stack<node_ptr_t<T>>& nodesStack
	) const;
};

template <typename T>
Node<T>::Node(const T& data) : n_data(data)
{}

template <typename T>
T Node<T>::GetData()
{
	return n_data;
}

template <typename T>
DirectedGraph<T>::DirectedGraph() : dg_nodes(std::list<node_ptr_t<T>>())
{}

template <typename T>
std::shared_ptr<Node<T>> DirectedGraph<T>::AddNode(const T& data)
{
	auto node_ptr = std::make_shared<Node<T>>(Node<T>(data));
	dg_nodes.push_back(node_ptr);

	return node_ptr;
}

template <typename T>
bool DirectedGraph<T>::TryAddEdge(node_ptr_t<T> from, node_ptr_t<T> to)
{
	if (isNodeInGraph(from) && isNodeInGraph(to))
	{
		auto endNodes = dg_edges[from];
		auto it = std::find(endNodes.begin(), endNodes.end(), to);

		if (it == endNodes.end())
			dg_edges[from].push_back(to);

		return true;
	}	
	return false;
}

template <typename T>
bool DirectedGraph<T>::isNodeInGraph(node_ptr_t<T> node) const
{
	auto it = std::find(dg_nodes.begin(), dg_nodes.end(), node);
	return it != dg_nodes.end();
}

template <typename T>
void DirectedGraph<T>::addNode(node_ptr_t<T> node)
{
	dg_nodes.push_back(node);
}

template <typename T>
DirectedGraph<T>::nodeMeta::nodeMeta(unsigned int index, unsigned int llink, bool onStack) :
	Index(index), Lowlink(llink), OnStack(onStack)
{}

template <typename T>
std::list<std::shared_ptr<Node<T>>> DirectedGraph<T>::GetNodes() const
{
	return dg_nodes;
}

template <typename T>
std::vector<node_ptr_t<T>> DirectedGraph<T>::FindNodes(const T& data) const
{
	auto vector = std::vector<node_ptr_t<T>>();

	for (auto it = dg_nodes.begin(); it != dg_nodes.end(); ++it)
		if ((*it)->GetData() == data) vector.push_back(*it);

	return vector;
}

template <typename T>
std::vector<DirectedGraph<T>> DirectedGraph<T>::FindStronglyConnectedComp() const
{
	unsigned int index = 0;
	std::stack<node_ptr_t<T>> nodeStack;
	auto meta = std::map<node_ptr_t<T>, std::shared_ptr<nodeMeta>>();
	auto result = std::vector<DirectedGraph<T>>();

	for (auto nodes_it = dg_nodes.begin(); nodes_it != dg_nodes.end(); ++nodes_it)
	{
		auto it = meta.find(*nodes_it);
		if (it == meta.end())
		{
			auto comp = stronglyConnected(*nodes_it, meta, index, nodeStack);
			if (comp.GetNodes().size() != 0)
				result.push_back(comp);
		}
	}
	return result;
}

template <typename T>
DirectedGraph<T> DirectedGraph<T>::stronglyConnected(
	node_ptr_t<T> node,
	std::map<node_ptr_t<T>, std::shared_ptr<nodeMeta>>& nodesMeta,
	unsigned int& index,
	std::stack<node_ptr_t<T>>& nodesStack
) const 
{
	nodeMeta* m = new nodeMeta(index, index, true);
	auto nMeta = std::shared_ptr<nodeMeta>(m);

	nodesMeta[node] = nMeta;
	nodesStack.push(node);
	index++;

	auto edge_it = dg_edges.find(node);
	if (edge_it != dg_edges.end())
	{
		auto nodes = edge_it->second;
		for (auto node_it = nodes.begin(); node_it != nodes.end(); ++node_it)
		{
			auto meta_it = nodesMeta.find(*node_it);
			if (meta_it == nodesMeta.end())
			{
				stronglyConnected(*node_it, nodesMeta, index, nodesStack);
				nMeta->Lowlink = std::min(nMeta->Lowlink, nodesMeta[*node_it]->Lowlink);
			}
			else
			{
				if (nMeta->OnStack)
					nMeta->Lowlink = std::min(nMeta->Lowlink, nodesMeta[*node_it]->Index);
			}
		}
	}

	// Creating new strongly connected graph component (scc)

	DirectedGraph<T> newComp = DirectedGraph<T>();
	node_ptr_t<T> stackNode;

	if (nMeta->Lowlink == nMeta->Index)
	{
		do
		{
			stackNode = nodesStack.top();
			nodesStack.pop();
			newComp.addNode(stackNode);
			nodesMeta[stackNode]->OnStack = false;
		} while (stackNode != node);
	}

	// Create connections in new scc

	auto nodes = newComp.GetNodes();

	for (auto node_it = nodes.begin(); node_it != nodes.end(); ++node_it)
	{
		auto successors_it = dg_edges.find(*node_it);
		if(successors_it != dg_edges.end())
		{
			auto successors = successors_it->second;
			for (auto it = nodes.begin(); it != nodes.end(); ++it)
			{
				auto find_it = std::find(successors.begin(), successors.end(), *it);
				if(find_it != successors.end())
				{
					newComp.TryAddEdge(*node_it, *it);
					successors.erase(find_it);
				}
			}
		}
	}
	
	return newComp;
}
